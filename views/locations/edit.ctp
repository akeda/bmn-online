<div class="<?=$this->params['controller']?> <?=$html->action?>">
<?php echo $form->create('Location');?>
	<fieldset>
 		<legend><?php __('Edit Location');?></legend>
        <table class="input">
            <tr>
                <td class="label-required"><?php echo __('Name of location');?>:</td>
                <td>
                <?php
                    echo $form->input('name', array(
                        'div'=>false, 'label' => false, 'maxlength' => 100, 'class'=>'required'
                    ));
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required"><?php echo __('Length (in meter)');?>:</td>
                <td>
                <?php
                    echo $form->input('length', array(
                        'div'=>false, 'label' => false, 'maxlength' => 11
                    ));
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required"><?php echo __('Width (in meter)');?>:</td>
                <td>
                <?php
                    echo $form->input('width', array(
                        'div'=>false, 'label' => false, 'maxlength' => 11
                    ));
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required"><?php echo __('Height (in meter)');?>:</td>
                <td>
                <?php
                    echo $form->input('height', array(
                        'div'=>false, 'label' => false, 'maxlength' => 11
                    ));
                ?>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                <?php
                    echo $form->submit(__('Save', true), array('div'=>false)) . '&nbsp;' . __('or', true) . '&nbsp;';
                    echo $html->link(
                        __('Delete', true),
                        array('action'=>'delete', $this->data['Location']['id']),
                        array('class'=>'del'),
                        sprintf(__('Are you sure you want to delete', true) . ' %s?', $this->data['Location']['name'])
                    ) . "&nbsp;" . __('or', true) . "&nbsp;";
                    echo $html->link(__('Back to index', true), array('action'=>'index'));
                ?>
                </td>
            </tr>
        </table>
	</fieldset>
</form>
</div>
