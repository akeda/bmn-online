<div class="<?php echo $this->params['controller']; ?> index">
<?php echo $this->element('tablegrid',
        array("fields" => array(
                "name" => array(
                    'title' => __("Name", true), 
                    'sortable' => true
                ),
                "width" => array(
                    'title' => __("Width", true),
                    'sortable' => false
                ),
                "height" => array(
                    'title' => __("Height", true),
                    'sortable' => false
                ),
                "length" => array(
                    'title' => __("Length", true),
                    'sortable' => false
                )
              ),
              "editable"  => "name"
        ));
?>
</div>
