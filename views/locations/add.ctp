<div class="<?=$this->params['controller']?> <?=$html->action?>">
<?php echo $form->create('Location');?>
	<fieldset>
 		<legend><?php __('Add Location');?></legend>
        <table class="input">
            <tr>
                <td class="label-required"><?php echo __('Name of location');?>:</td>
                <td>
                <?php
                    echo $form->input('name', array(
                        'div'=>false, 'label' => false, 'maxlength' => 100, 'class'=>'required'
                    ));
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required"><?php echo __('Length (in meter)');?>:</td>
                <td>
                <?php
                    echo $form->input('length', array(
                        'div'=>false, 'label' => false, 'maxlength' => 11
                    ));
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required"><?php echo __('Width (in meter)');?>:</td>
                <td>
                <?php
                    echo $form->input('width', array(
                        'div'=>false, 'label' => false, 'maxlength' => 11
                    ));
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required"><?php echo __('Height (in meter)');?>:</td>
                <td>
                <?php
                    echo $form->input('height', array(
                        'div'=>false, 'label' => false, 'maxlength' => 11
                    ));
                ?>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                <?php
                    echo $form->submit(__('Add', true), array('div'=>false)) . '&nbsp;' . __('or', true) . '&nbsp;';
                    echo $html->link(__('Back to index', true), array('action'=>'index'));
                ?>
                </td>
            </tr>
        </table>
	</fieldset>
</form>
</div>
