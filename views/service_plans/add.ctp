<div class="<?=$this->params['controller']?> <?=$html->action?>">
<?php echo $form->create('ServicePlan');?>
	<fieldset>
 		<legend><?php __('Add Service Plan');?></legend>
        <table class="input">
            <tr>
                <td class="label-required"><?php echo __('Name');?>:</td>
                <td>
                <?php
                    echo $form->input('name', array(
                        'div'=>false, 'label' => false, 'maxlength' => 20, 'class'=>'required'
                    ));
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required"><?php echo __('Period');?>:</td>
                <td>
                <?php
                    echo $form->select('period', $periods, null, array(
                        'class' => 'inputText',
                        'empty' => 'Pilih periode'
                    ));
                    echo $form->isFieldError('ServicePlan.period') ? $form->error('ServicePlan.period') : '';
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required"><?php echo __('Total Period');?>:</td>
                <td>
                <?php
                    echo $form->input('total_period', array(
                        'div'=>false, 'label' => false, 'maxlength' => 3
                    ));
                ?>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                <?php
                    echo $form->submit(__('Add', true), array('div'=>false)) . '&nbsp;' . __('or', true) . '&nbsp;';
                    echo $html->link(__('Back to index', true), array('action'=>'index'));
                ?>
                </td>
            </tr>
        </table>
	</fieldset>
</form>
</div>
