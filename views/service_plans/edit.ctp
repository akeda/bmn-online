<div class="<?=$this->params['controller']?> <?=$html->action?>">
<?php echo $form->create('ServicePlan');?>
	<fieldset>
 		<legend><?php __('Edit Service Plan');?></legend>
        <table class="input">
            <tr>
                <td class="label-required"><?php echo __('Name');?>:</td>
                <td>
                <?php
                    echo $form->input('name', array(
                        'div'=>false, 'label' => false, 'maxlength' => 20, 'class'=>'required'
                    ));
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required"><?php echo __('Period');?>:</td>
                <td>
                <?php
                    echo $form->select('period', $periods, null, array(
                        'class' => 'inputText',
                        'empty' => 'Pilih periode'
                    ));
                    echo $form->isFieldError('ServicePlan.period') ? $form->error('ServicePlan.period') : '';
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required"><?php echo __('Total Period');?>:</td>
                <td>
                <?php
                    echo $form->input('total_period', array(
                        'div'=>false, 'label' => false, 'maxlength' => 3
                    ));
                ?>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                <?php
                    echo $form->submit(__('Save', true), array('div'=>false)) . '&nbsp;' . __('or', true) . '&nbsp;';
                    echo $html->link(
                        __('Delete', true),
                        array('action'=>'delete', $this->data['ServicePlan']['id']),
                        array('class'=>'del'),
                        sprintf(__('Are you sure you want to delete', true) . ' %s?', $this->data['ServicePlan']['name'])
                    ) . "&nbsp;" . __('or', true) . "&nbsp;";
                    echo $html->link(__('Back to index', true), array('action'=>'index'));
                ?>
                </td>
            </tr>
        </table>
	</fieldset>
</form>
</div>
