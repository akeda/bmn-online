<div class="<?php echo $this->params['controller']; ?> index">
<?php echo $this->element('tablegrid',
        array("fields" => array(
                "name" => array(
                    'title' => __("Name", true), 
                    'sortable' => true
                ),
                "period" => array(
                    'title' => __("Periode", true),
                    'sortable' => false
                ),
                "total_period" => array(
                    'title' => __("Total Periode", true),
                    'sortable' => false
                )
              ),
              "editable"  => "name"
        ));
?>
</div>
