<?php echo $html->scriptBlock($items_js);?>
<?php echo $html->script('item_services.js?20102711', false);?>
<div class="<?=$this->params['controller']?> <?=$html->action?>">
<?php echo $form->create('ItemService');?>
	<fieldset>
 		<legend><?php __('Tambah Barang Yang Akan Diservice');?></legend>
        <table class="input">
            <tr>
                <td class="label-required"><?php echo __('Nama Barang');?>:</td>
                <td>
                <?php
                    echo $form->select('asset_id', $items, null, array(
                        'div' => false, 'label' => false, 'empty' => false
                    ));
                    echo ($form->isFieldError('ItemService.asset_id')) ?
                         $form->error('ItemService.asset_id') : '';
                ?>
                <table>
                    <tr>
                        <td>No. Inventaris</td>
                        <td><span class="asset_inventory_no"></span></td>
                    </tr>
                    <tr>
                        <td>Lokasi</td>
                        <td><span class="asset_location"></span></td>
                    </tr>
                    <tr>
                        <td>Total</td>
                        <td><span class="asset_total"></span></td>
                    </tr>
                </table>
                </td>
            </tr>
            <tr>
                <td class="label-required"><?php echo __('Diservice karena');?>:</td>
                <td>
                <?php
                    echo $form->select('service_for', $service_for, null, array(
                        'div' => false, 'label' => false, 'empty' => false
                    ));
                    echo ($form->isFieldError('ItemService.service_for')) ? 
                         $form->error('ItemService.service_for') : '';
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required"><?php echo __('Teknisi');?>:</td>
                <td>
                <?php
                    echo $form->input('technician', array(
                        'div'=>false, 'label' => false
                    ));
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required"><?php echo __('Diservice pada tanggal');?>:</td>
                <td>
                <?php
                    echo $form->input('service_date', array(
                        'div'=>false, 'label' => false
                    ));
                    echo ($form->isFieldError('ItemService.service_date')) ? 
                         $form->error('ItemService.service_date') : '';
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required"><?php echo __('Uraian service');?>:</td>
                <td>
                <?php
                    echo $form->input('description', array(
                        'div'=>false, 'label' => false, 'rows' => 3
                    ));
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required"><?php echo __('Jumlah item yang diservice');?>:</td>
                <td>
                <?php
                    echo $form->input('total_item_serviced', array(
                        'div'=>false, 'label' => false, 'maxlength' => 5, 'size' => 5
                    ));
                ?> dari total <span class="asset_total"></span>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                <?php
                    echo $form->submit(__('Add', true), array('div'=>false)) . '&nbsp;' . __('or', true) . '&nbsp;';
                    echo $html->link(__('Back to index', true), array('action'=>'index'));
                ?>
                </td>
            </tr>
        </table>
	</fieldset>
</form>
</div>
