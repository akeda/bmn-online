<div class="<?php echo $this->params['controller']; ?> index">
<?php echo $this->element('tablegrid',
        array("fields" => array(
                "asset_item_name" => array(
                    'title' => __("Nama Barang", true), 
                    'sortable' => true
                ),
                "asset_item_inventory_no" => array(
                    'title' => __("No. Inventaris", true),
                    'sortable' => true
                ),
                "asset_item_location" => array(
                    'title' => __("Lokasu Barang", true),
                    'sortable' => false
                ),
                "service_for" => array(
                    'title' => __("Diservice karena", true),
                    'sortable' => true
                ),
                "technician" => array(
                    'title' => __("Teknisi", true),
                    'sortable' => false
                ),
                "service_date" => array(
                    'title' => __("Diservice pada tanggal", true),
                    'sortable' => false
                ),
                "description" => array(
                    'title' => __("Uraian Service", true),
                    'sortable' => false
                ),
                "total_item_serviced" => array(
                    'title' => __("Jumlah Item yang diservice", true),
                    'sortable' => false
                ),
                "created" => array(
                    'title' => __("Created on", true),
                    'sortable' => false
                ),
                "created_by" => array(
                    'title' => __("Created by", true),
                    'sortable' => false
                ),
              ),
              "editable"  => "asset_item_name",
              'assoc' => array(
                'created_by' => array(
                    'model' => 'User',
                    'field' => 'name'
                )
              ),
              "displayedAs" => array(
                'service_date' => 'date',
                'created' => 'datetime'
              )
        ));
?>
</div>
