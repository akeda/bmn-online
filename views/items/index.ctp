<div class="<?php echo $this->params['controller']; ?> index">
<?php echo $this->element('tablegrid',
        array("fields" => array(
                "name" => array(
                    'title' => __("Name", true), 
                    'sortable' => true
                ),
                "type" => array(
                    'title' => __("Type/Brand", true),
                    'sortable' => false
                ),
                "production_year" => array(
                    'title' => __("Production year", true),
                    'sortable' => false
                ),
                "machine_number" => array(
                    'title' => __("Machine number", true),
                    'sortable' => false
                ),
                "morning_power_usage" => array(
                    'title' => __("Morning power usage", true),
                    'sortable' => false
                ),
                "night_power_usage" => array(
                    'title' => __("Night power usage", true),
                    'sortable' => false
                ),
                "unit_id" => array(
                    'title' => __("Unit", true),
                    'sortable' => false
                ),
                "price_per_unit" => array(
                    'title' => __("Price per unit", true),
                    'sortable' => false
                ),
                "length" => array(
                    'title' => __("Length", true),
                    'sortable' => false
                ),
                "width" => array(
                    'title' => __("Width", true),
                    'sortable' => false
                ),
                "height" => array(
                    'title' => __("Height", true),
                    'sortable' => false
                ),
                "service_plan_id" => array(
                    'title' => __("Service plan", true),
                    'sortable' => false
                ),
                "provider_id" => array(
                    'title' => __("Provider", true),
                    'sortable' => false
                ),
                "created" => array(
                    'title' => __("Created on", true),
                    'sortable' => false
                ),
                "created_by" => array(
                    'title' => __("Created by", true),
                    'sortable' => false
                ),
              ),
              "editable"  => "name",
              'assoc' => array(
                'created_by' => array(
                    'model' => 'User',
                    'field' => 'name'
                ),
                'unit_id' => array(
                    'model' => 'Unit',
                    'field' => 'name'
                ),
                'service_plan_id' => array(
                    'model' => 'ServicePlan',
                    'field' => 'name'
                ),
                'provider_id' => array(
                    'model' => 'Provider',
                    'field' => 'name'
                )
              ),
              "displayedAs" => array(
                'transaction_date' => 'date',
                'created' => 'datetime',
                'price_per_unit' => 'money'
              )
        ));
?>
</div>
