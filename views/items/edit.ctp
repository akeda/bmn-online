<div class="<?=$this->params['controller']?> <?=$html->action?>">
<?php echo $form->create('Item');?>
	<fieldset>
 		<legend><?php __('Edit Item');?></legend>
        <table class="input">
            <tr>
                <td class="label-required"><?php echo __('Name');?>:</td>
                <td>
                <?php
                    echo $form->input('name', array(
                        'div'=>false, 'label' => false, 'maxlength' => 100, 'class'=>'required'
                    ));
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required"><?php echo __('Type/Brand');?>:</td>
                <td>
                <?php
                    echo $form->input('type', array(
                        'div'=>false, 'label' => false, 'maxlength' => 100
                    ));
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required"><?php echo __('Production year');?>:</td>
                <td>
                <?php
                    echo $form->input('production_year', array(
                        'div'=>false, 'label' => false, 'maxlength' => 4
                    ));
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required"><?php echo __('Machine number');?>:</td>
                <td>
                <?php
                    echo $form->input('machine_number', array(
                        'div'=>false, 'label' => false, 'maxlength' => 20
                    ));
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required"><?php echo __('Morning power usage');?>:</td>
                <td>
                <?php
                    echo $form->input('morning_power_usage', array(
                        'div'=>false, 'label' => false, 'maxlength' => 2
                    ));
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required"><?php echo __('Night power usage');?>:</td>
                <td>
                <?php
                    echo $form->input('night_power_usage', array(
                        'div'=>false, 'label' => false, 'maxlength' => 2
                    ));
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required"><?php echo __('Unit');?>:</td>
                <td>
                <?php
                    echo $form->select('unit_id', $units, null, array(
                        'class' => 'inputText',
                        'empty' => 'Pilih satuan'
                    ));
                    echo $form->isFieldError('Item.unit_id') ? $form->error('Item.unit_id') : '';
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required"><?php echo __('Price per unit');?>:</td>
                <td>
                <?php
                    echo $form->input('price_per_unit', array(
                        'div'=>false, 'label' => false, 'maxlength' => 18
                    ));
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required"><?php echo __('Length');?>:</td>
                <td>
                <?php
                    echo $form->input('length', array(
                        'div'=>false, 'label' => false, 'maxlength' => 11
                    ));
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required"><?php echo __('Width');?>:</td>
                <td>
                <?php
                    echo $form->input('width', array(
                        'div'=>false, 'label' => false, 'maxlength' => 11
                    ));
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required"><?php echo __('Height');?>:</td>
                <td>
                <?php
                    echo $form->input('height', array(
                        'div'=>false, 'label' => false, 'maxlength' => 11
                    ));
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required"><?php echo __('Service plan');?>:</td>
                <td>
                <?php
                    echo $form->select('service_plan_id', $service_plans, null, array(
                        'class' => 'inputText',
                        'empty' => 'Pilih rencana service'
                    ));
                    echo $form->isFieldError('Item.service_plan_id') ? $form->error('Item.service_plan_id') : '';
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required"><?php echo __('Provider');?>:</td>
                <td>
                <?php
                    echo $form->select('provider_id', $providers, null, array(
                        'class' => 'inputText',
                        'empty' => 'Pilih penyedia'
                    ));
                    echo $form->isFieldError('Item.provider_id') ? $form->error('Item.provider_id') : '';
                ?>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                <?php
                    echo $form->submit(__('Save', true), array('div'=>false)) . '&nbsp;' . __('or', true) . '&nbsp;';
                    echo $html->link(
                        __('Delete', true),
                        array('action'=>'delete', $this->data['Item']['id']),
                        array('class'=>'del'),
                        sprintf(__('Are you sure you want to delete', true) . ' %s?', $this->data['Item']['name'])
                    ) . "&nbsp;" . __('or', true) . "&nbsp;";
                    echo $html->link(__('Back to index', true), array('action'=>'index'));
                ?>
                </td>
            </tr>
        </table>
	</fieldset>
</form>
</div>
