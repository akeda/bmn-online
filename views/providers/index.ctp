<div class="<?php echo $this->params['controller']; ?> index">
<?php echo $this->element('tablegrid',
        array("fields" => array(
                "name" => array(
                    'title' => __("Name", true), 
                    'sortable' => true
                ),
                "address" => array(
                    'title' => __("Width", true),
                    'sortable' => false
                )
              ),
              "editable"  => "name"
        ));
?>
</div>
