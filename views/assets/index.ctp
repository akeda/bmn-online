<div class="<?php echo $this->params['controller']; ?> index">
<?php echo $this->element('tablegrid',
        array("fields" => array(
            "item_id" => array(
                'title' => __("Nama barang", true), 
                'sortable' => true
            ),
            "inventory_no" => array(
                'title' => __("No. Inventaris", true), 
                'sortable' => true
            ),
            "receipt_no" => array(
                'title' => __("No. Bukti Penerimaan", true), 
                'sortable' => true
            ),
            "receipt_date" => array(
                'title' => __("Tgl. Bukti Penerimaan", true), 
                'sortable' => true
            ),
            "receipt_no_spk" => array(
                'title' => __("No. SPK", true), 
                'sortable' => true
            ),
            "sp2d_no" => array(
                'title' => __("No. SP2D", true), 
                'sortable' => false
            ),
            "sp2d_date" => array(
                'title' => __("Tgl. SP2D", true),
                'sortable' => true
            ),
            "sp2d_klb" => array(
                'title' => __("KLB", true),
                'sortable' => true
            ),
            "spm" => array(
                'title' => __("Nilai SPM", true),
                'sortable' => true
            ),
            "created_by" => array(
                'title' => __("Created By", true),
                'sortable' => false
            ),
            "created" => array(
                'title' => __("Created On", true),
                'sortable' => false
            )
          ),
          "editable"  => "item_id",
          "assoc" => array(
            'created_by' => array(
                'model' => 'User',
                'field' => 'name'
            ),
            'item_id' => array(
                'model' => 'Item',
                'field' => 'name'
            )
          ),
          'displayedAs' => array(
            'receipt_date' => 'date',
            'sp2d_date' => 'date',
            'created' => 'datetime'
          )
        ));
?>
</div>
