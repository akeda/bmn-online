<h1>Buku Induk</h1>
<table>
    <thead>
        <tr>
            <th colspan="3">Bukti Penerimaan</th>
            <th rowspan="2">Asal Perolehan</th>
            <th rowspan="2">Nama/Jenis Barang</th>
            <th rowspan="2">Type/Mark</th>
            <th rowspan="2">Tahun Pembuatan</th>
            <th rowspan="2">Nomor Mesin</th>
            <th colspan="2">Daya Listrik</th>
            <th rowspan="2">Rencana Service</th>
            <th rowspan="2">Kuantitas</th>
            <th rowspan="2">Satuan</th>
            <th colspan="2">Harga (Rp)</th>
            <th colspan="3">Bukti SP2D</th>
            <th rowspan="2">Nilai SPM (Rp)</th>
            <th rowspan="2">No. Inven Barang</th>
            <th rowspan="2">Kondisi BRB/RB</th>
            <th rowspan="2">Lokasi Barang</th>
            <th rowspan="2">Unit Kerja</th>
        </tr>
        <tr>
            <th>Nomor</th>
            <th>Tanggal</th>
            <th>Nomor SPK</th>
            <th>Malam</th>
            <th>Siang</th>
            <th>Satuan</th>
            <th>Jumlah</th>
            <th>Nomor</th>
            <th>Tanggal</th>
            <th>KLB</th>
        </tr>
    </thead>
    <tbody>
    <?php if ( !empty($records) ) : ?>
    <?php foreach ( $records as $record ) : ?>
        <tr>
            <td><?php echo $record['receipt_no'];?></td>
            <td><?php echo $record['receipt_date'];?></td>
            <td><?php echo $record['receipt_no_spk'];?></td>
            <td><?php echo $record['provider'];?></td>
            <td><?php echo $record['item_name'];?></td>
            <td><?php echo $record['item_type'];?></td>
            <td><?php echo $record['item_year'];?></td>
            <td><?php echo $record['item_machine'];?></td>
            <td><?php echo $record['item_morning_power'];?></td>
            <td><?php echo $record['item_night_power'];?></td>
            <td><?php echo $record['service_plan'];?></td>
            <td><?php echo $record['total'];?></td>
            <td><?php echo $record['item_unit'];?></td>
            <td><?php echo number_format($record['item_price'], 2, '.', ',');?></td>
            <td><?php echo number_format($record['item_price_total'], 2, '.', ',');?></td>
            
            <td><?php echo $record['sp2d_no'];?></td>
            <td><?php echo $record['sp2d_date'];?></td>
            <td><?php echo $record['sp2d_klb'];?></td>
            <td><?php echo $record['spm'];?></td>
            <td><?php echo $record['inventory_no'];?></td>
            <td><?php echo '-';?></td>
            <td><?php echo $record['item_location'];?></td>
            <td><?php echo $record['item_unit_code'];?></td>
        </tr>
    <?php endforeach; ?>
    <?php endif; ?>
    </tbody>
</table>
