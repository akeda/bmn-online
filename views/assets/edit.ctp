<?php echo $html->scriptBlock($item_price_js);?>
<?php echo $html->css('assets', 'stylesheet', array('inline' => false));?>
<div class="<?=$this->params['controller']?> <?=$html->action?>">
<?php echo $form->create('Asset', array('action' => 'edit'));?>
	<fieldset>
 		<legend><?php __('Edit Asset');?></legend>
        <table class="input">
            <tr>
                <td>
                    <table class="input">
                        <tr>
                            <td class="label-required">Nama barang</td>
                            <td>
                                <?php 
                                    echo $form->select('item_id', $items, null,
                                        array(
                                            'div' => false, 'label' => false, 'class' => 'inputText',
                                            'empty' => false
                                        )
                                    );
                                    echo ($form->isFieldError('Asset.item_id')) ? 
                                          $form->error('Asset.item_id') : '';
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="label-required">Jumlah barang</td>
                            <td>
                                <?php 
                                    echo $form->input('total', array(
                                            'div' => false, 'label' => false, 'class' => 'inputText',
                                            'size' => 5, 'maxlength' => 5
                                        )
                                    );
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="label-required">Lokasi</td>
                            <td>
                                <?php 
                                    echo $form->select(
                                        'location_id', $locations, null,
                                        array(
                                            'div' => false, 'label' => false,
                                            'class' => 'inputText', 'empty' => false
                                        )
                                    );
                                    echo ($form->isFieldError('Asset.location_id')) ?
                                          $form->error('Asset.location_id') : '';
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="label-required">Unit kerja</td>
                            <td>
                                <?php 
                                    echo $form->select(
                                        'unit_code_id', $unit_codes, null,
                                        array(
                                            'div' => false, 'label' => false,
                                            'class' => 'inputText', 'empty' => false
                                        )
                                    );
                                    echo ($form->isFieldError('Asset.unit_code_id')) ?
                                          $form->error('Asset.unit_code_id') : '';
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="label-required">No. inventaris</td>
                            <td>
                                <?php
                                    echo $form->input('inventory_no', array(
                                            'div' => false, 'label' => false, 'class' => 'inputText'
                                        )
                                    );
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="label-required">No. Bukti Penerimaan</td>
                            <td>
                                <?php 
                                    echo $form->input('receipt_no', array(
                                        'div' => false, 'label' => false, 'class' => 'inpuText'
                                    ));
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="label-required">Tgl. Bukti Penerimaan</td>
                            <td>
                                <?php 
                                    echo $form->input('receipt_date', array(
                                        'div' => false, 'label' => false,
                                        'class' => 'inputText'
                                    ));
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="label-required">No. SPK</td>
                            <td>
                                <?php 
                                    echo $form->input('receipt_no_spk', array(
                                        'div' => false, 'label' => false, 'class' => 'inpuText'
                                    ));
                                ?>
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                    <table class="input">
                        <tr>
                            <td class="label-required"><?php echo __('No. SP2D', true);?></td>
                            <td>
                                <?php 
                                    echo $form->input('sp2d_no', array(
                                        'div' => false, 'label' => false, 'class' => 'inpuText'
                                    ));
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="label-required"><?php echo __('Tgl. SP2D', true);?></td>
                            <td>
                                <?php 
                                    echo $form->input('sp2d_date', array(
                                        'div' => false, 'label' => false,
                                        'class' => 'inputText'
                                    ));
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="label-required"><?php echo __('KLB', true);?></td>
                            <td>
                                <?php 
                                    echo $form->input('sp2d_klb', array(
                                        'div' => false, 'label' => false, 'class' => 'inpuText'
                                    ));
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="label-required"><?php echo __('Nilai SPM', true);?></td>
                            <td>
                                <?php 
                                    echo $form->input('spm', array(
                                        'div' => false, 'label' => false, 'class' => 'inpuText'
                                    ));
                                ?>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                <?php
                    echo $form->submit('Simpan Asset ini', array('div'=>false)) . "&nbsp;" . __('or', true) . "&nbsp;";
                    echo $html->link(__('Back', true), array('action'=>'index'), array('class'=>'back'));
                ?>
                </td>
            </tr>
        </table>
	</fieldset>
</form>	
</div>
