<?php if ( $show_form ): // show form to select date and loations ?>
<?php echo $form->create('Asset');?>
<fieldset>
<legend>Daftar Inventaris Ruangan</legend>
<table class="input">
    <tr>
        <td>Ruangan / Lokasi</td>
        <td>
        <?php
            // locations
            echo $form->select('location', $locations, null, array(
                'empty' => false, 'name' => 'data[location]'
            ));
        ?>
        </td>
    </tr>
    <tr>
        <td>Keadaan Tangal</td>
        <td>
        <?php
            echo $form->input('date', array(
                'type' => 'date', 'div' => false, 'label' => false
            ));
        ?>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <input type="submit" value="Print" />
        </td>
    </tr>
</table>
</fieldset>
</form>
<?php else: // printml report?>
<h1>Buku Induk</h1>

<center>
<table class="noborder left">
    <tr>
        <td>Nama Ruangan</td>
        <td><strong><?php echo $location_name;?></strong></td>
        <td>Luas Ruangan</td>
        <td>
        <?php
            echo ($wide) ? $wide . ' m<sup>2</sup>' : '';
        ?>
        </td>
    </tr>
    <tr>
        <td>Kode Ruangan</td>
        <td></td>
        <td colspan="2">&nbsp;</td>
    </tr>
    <tr>
        <td>Keadaan Tanggal</td>
        <td><?php echo $time->format('d/m/Y', $date);?></td>
        <td colspan="2">&nbsp;</td>
    </tr>
</table>

<table>
    <thead>
        <tr>
            <th>No</th>
            <th>Nama Barang</th>
            <th>Merk / Type</th>
            <th>Tahun Pembuatan</th>
            <th>Tahun Pembelian</th>
            <th>Jumlah</th>
            <th>Kondisi</th>
        </tr>
    </thead>
    
    <tbody>
        
    </tbody>
</table>
</center>
<?php endif;?>
