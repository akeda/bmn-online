<?php
class Item extends AppModel {
    var $validate = array(
        'name' => array(
            'required' => array(
                'required' => true,
                'allowEmpty' => false,
                'rule' => '/^\w+[\w\s]?/',
                'message' => 'This field cannot be left blank and must be alphanumeric'
            ),
            'maxlength' => array(
                'rule' => array('maxLength', 100),
                'message' => 'Maximum characters is 100 characters'
            )
        ),
        'unit_id' => array(
            'required' => true,
            'allowEmpty' => false,
            'rule' => 'validUnit',
            'message' => 'Invalid value'
        ),
        'price_per_unit' => array(
            'required' => true,
            'allowEmpty' => false,
            'rule' => 'numeric',
            'message' => 'This field must be in numeric'
        )
    );
    
    var $belongsTo = array(
        'Unit' => array(
            'className' => 'Unit',
            'foreignKey' => 'unit_id',
            'fields' => array('id', 'name')
        ),
        'ServicePlan' => array(
            'className' => 'ServicePlan',
            'foreignKey' => 'service_plan_id',
            'fields' => array('id', 'name')
        ),
        'Provider' => array(
            'className' => 'Provider',
            'foreignKey' => 'provider_id',
            'fields' => array('id', 'name')
        ),
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'created_by',
            'fields' => array('id', 'name')
        )
    );
    
    function validUnit($field) {
        $units = $this->Unit->find('list', array(
            'fields' => array('id', 'id')
        ));
        
        if ( !in_array($field['unit_id'], $units) ) {
            return false;
        }
        
        return true;
    }
}
?>
