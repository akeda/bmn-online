<?php
class ItemService extends AppModel {
    var $name = 'ItemService';
    
    const SERVICE_FOR_DAMAGE      = 1;
    const SERVICE_FOR_REPAIR      = 2;
    const SERVICE_FOR_MAINTENANCE = 3;
    
    var $validate = array(
        'asset_id' => array(
            'valid' => array(
                'rule' => 'vAsset',
                'message' => 'Barang tidak terdaftar sebagai asset'
            )
        ),
        'service_for' => array(
            'rule' => array('inList', array(1, 2, 3)),
            'message' => 'Masukkan sesuai pilihan'
        ),
        'service_date' => array(
            'rule' => 'date',
            'message' => 'Masukkan tanggal yang valid'
        ),
        'total_item_serviced' => array(
            'valid' => array(
                'rule' => 'numeric',
                'message' => 'Masukkan angka yang valid',
                'last' => true
            ),
            'max' => array(
                'rule' => 'vTotal',
                'message' => 'Total melebihi jumlah asset yang tersedia'
            )
        )
    );
    
    var $belongsTo = array(
        'Asset',
        'User' => array(
            'foreignKey' => 'created_by'
        )
    );
    
    function paginate($conditions, $fields, $order, $limit, $page = 1, $recursive = null, $extra = array()) {
        $this->Behaviors->attach('Containable');
        $fields = array(
            'id', 'asset_id', 'service_for', 'technician', 'service_date', 
            'total_item_serviced', 'description', 'created', 'created_by'
        );
        $contain = array(
            'Asset' => array(
                'fields' => array('inventory_no', 'total'),
                'Item' => array(
                    'fields' => array('name', 'type', 'production_year', 'machine_number')
                ),
                'Location' => array(
                    'fields' => array('name')
                )
            ),
            'User' => array(
                'fields' => array('name')
            )
        );
        $recursive = 2;
        $records = $this->find('all', compact(
            'conditions', 'fields', 'order', 'limit',
            'page', 'recursive', 'group', 'contain'
            )
        );
        foreach ( $records as $key => $record ) {
            $item_desc = '';
            if ( !empty($record['Asset']['Item']['type']) ) {
                $item_desc .=  '(' . $record['Asset']['Item']['type'];
            }
            if ( !empty($record['Asset']['Item']['machine_number']) ) {
                if ( empty($item_desc) ) {
                    $item_desc .=  '(';
                } else {
                    $item_desc .=  ' - ';
                }
                $item_desc .=  $record['Asset']['Item']['machine_number'];
            }
            if ( !empty($item_desc) ) {
                $item_desc = ' ' . $item_desc . ')';
            }
            $records[$key]['ItemService']['asset_item_name'] = $record['Asset']['Item']['name'] . $item_desc;
            $records[$key]['ItemService']['asset_item_inventory_no'] = $record['Asset']['inventory_no'];
            $records[$key]['ItemService']['asset_item_location'] = $record['Asset']['Location']['name'];
            $records[$key]['ItemService']['service_for'] = $this->__niceServiceFor($record['ItemService']['service_for']);
        }
        
        return $records;
    }
    
    function vAsset($field) {
        $asset_items = $this->Asset->find('list', array(
            'fields' => array('id'),
            'recursive' => -1
        ));
        
        if ( !in_array($field['asset_id'], $asset_items) ) {
            return false;
        }
        
        return true;
    }
    
    function vTotal($field) {
        $id = $this->data['ItemService']['asset_id'];
        
        $asset_items = $this->Asset->find('first', array(
            'conditions' => array('id' => $id),
            'fields' => array('total'),
            'recursive' => -1
        ));
        
        if ( $field['total_item_serviced'] <= $asset_items['Asset']['total'] ) {
            return true;
        }
        
        return false;
    }
    
    function __niceServiceFor($id) {
        $resp = '';
        switch ( $id ) {
            case self::SERVICE_FOR_DAMAGE:
                $resp = 'Rusak berat';
                break;
            case self::SERVICE_FOR_REPAIR:
                $resp = 'Rusak ringan / butuh perbaikan';
                break;
            case SERVICE_FOR_MAINTENANCE:
            default:
                $resp = 'Pemeliharaan rutin';
        }
        
        return $resp;
    }
}
?>
