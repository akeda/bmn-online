<?php
class ServicePlan extends AppModel {
    var $period = array('day', 'month');
    var $validate = array(
        'name' => array(
            'required' => array(
                'required' => true,
                'allowEmpty' => false,
                'rule' => '/^\w+[\w\s]?/',
                'message' => 'This field cannot be left blank and must be alphanumeric'
            ),
            'maxlength' => array(
                'rule' => array('maxLength', 20),
                'message' => 'Maximum characters is 100 characters'
            )
        ),
        'period' => array(
            'valid' => array(
                'required' => true,
                'allowEmpty' => false,
                'rule' => 'validPeriod',
                'message' => 'Invalid value'
            )
        ),
        'total_period' => array(
            'required' => true,
            'allowEmpty' => false,
            'rule' => 'numeric',
            'message' => 'This field must be in numeric'
        )
    );
    
    function validPeriod($field) {
        if ( !in_array($field['period'], $this->period) ) {
            return false;
        }
        
        return true;
    }
}
?>
