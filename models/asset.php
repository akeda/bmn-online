<?php
class Asset extends AppModel {
    var $validate = array(
        'item_id' => array(
            'valid' => array(
                'rule' => 'vItem',
                'message' => 'Wajib diisi dengan daftar barang yang ada'
            )
        ),
        'inventory_no' => array(
            'required' => array(
                'required' => true,
                'allowEmpty' => false,
                'rule' => 'notEmpty',
                'message' => 'No. inventory wajib diisi'
            ),
            'valid' => array(
                'rule' => array('unique', 'inventory_no'),
                'message' => 'No. inventory sudah terpakai'
            )
        ),
        'location_id' => array(
            'valid' => array(
                'rule' => 'vLocation',
                'message' => 'Wajib diisi dengan daftar lokasi yang ada'
            )
        ),
        'unit_code_id' => array(
            'valid' => array(
                'rule' => 'vUnitCode',
                'message' => 'Wajib diisi dengan daftar unit kerja yang ada'
            )
        ),
        'receipt_no' => array(
            'required' => true,
            'allowEmpty' => false,
            'rule' => 'notEmpty',
            'message' => 'Wajib diisi'
        ),
        'receipt_date' => array(
            'required' => true,
            'allowEmpty' => false,
            'rule' => 'date',
            'message' => 'Wajib diisi dengan tanggal'
        ),
        'receipt_no_spk' => array(
            'required' => true,
            'allowEmpty' => false,
            'rule' => 'notEmpty',
            'message' => 'Wajib diisi'
        ),
        'sp2d_no' => array(
            'required' => true,
            'allowEmpty' => false,
            'rule' => 'notEmpty',
            'message' => 'Wajib diisi'
        ),
        'sp2d_date' => array(
            'required' => true,
            'allowEmpty' => false,
            'rule' => 'date',
            'message' => 'Wajib diisi dengan tanggal'
        ),
        'sp2d_klb' => array(
            'required' => true,
            'allowEmpty' => false,
            'rule' => 'notEmpty',
            'message' => 'Wajib diisi'
        ),
        'spm' => array(
            'required' => true,
            'allowEmpty' => false,
            'rule' => 'numeric',
            'message' => 'Wajib diisi dengan angka'
        )
    );

    var $belongsTo = array(
        'Item', 'Location', 'UnitCode',
        'User' => array(
            'foreignKey' => 'created_by'
        )
    );
    
    function vItem($field) {
        return $this->Item->find('count', array(
            'conditions' => array(
                'Item.id' => $field['item_id']
            )
        )) > 0;
    }
    
    function vLocation($field) {
        return $this->Location->find('count', array(
            'conditions' => array(
                'Location.id' => $field['location_id']
            )
        )) > 0;
    }
    
    function vUnitCode($field) {
        return $this->UnitCode->find('count', array(
            'conditions' => array(
                'UnitCode.id' => $field['unit_code_id']
            )
        )) > 0;
    }
}
?>
