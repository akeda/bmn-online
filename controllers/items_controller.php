<?php
class ItemsController extends AppController {
    var $pageTitle = 'Items';
    
    function add() {
        parent::add();
        $this->__setAdditionals();
    }
    
    function edit($id) {
        parent::edit($id);
        $this->__setAdditionals();
    }
    
    function __setAdditionals() {
        $units = $this->Item->Unit->find('list', array(
            'fields' => array('id', 'name')
        ));
        $this->set('units', $units);
        
        $service_plans = $this->Item->ServicePlan->find('list', array(
            'fields' => array('id', 'name')
        ));
        $this->set('service_plans', $service_plans);
        
        $providers = $this->Item->Provider->find('list', array(
            'fields' => array('id', 'name')
        ));
        $this->set('providers', $providers);
    }
}
?>
