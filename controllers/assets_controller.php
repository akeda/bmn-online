<?php
class AssetsController extends AppController {
    var $pageTitle = 'Assets';
    
    function index() {
        $this->paginate['Asset']['order'] = array(
            'Asset.receipt_date' => 'ASC',
            'Asset.id' => 'ASC'
        );
        parent::index();
    }

    function add() {
        $this->__setAdditionals();
		parent::add();
	}
    
    function edit($id = null) {
        $this->__setAdditionals(1, $id);
        parent::edit($id);
    }
    
    function __setAdditionals($edit = false, $id = null) {
        // items
        $items = $this->Asset->Item->find('all', array(
            'fields' => array('id', 'name', 'type', 'machine_number', 'price_per_unit'),
            'recursive' => 0
        ));
        $_items = array();
        $item_price = array();
        foreach ( $items as $key => $item ) {
            $item_desc = '';
            if ( !empty($item['Item']['type']) ) {
                $item_desc .=  '(' . $item['Item']['type'];
            }
            if ( !empty($item['Item']['machine_number']) ) {
                if ( empty($item_desc) ) {
                    $item_desc .=  '(';
                } else {
                    $item_desc .=  ' - ';
                }
                $item_desc .=  $item['Item']['machine_number'];
            }
            if ( !empty($item_desc) ) {
                $item_desc = ' ' . $item_desc . ')';
            }
            $_items[$item['Item']['id']] = $item['Item']['name'] . $item_desc;
            $item_price[$item['Item']['id']] = $item['Item']['price_per_unit'];
        }
        $this->set('items', $_items);
        $this->set('item_price', $item_price);
        $this->set('item_price_js', 'var item_price = ' . json_encode($item_price) . ';');
        
        // locations
        $locations = $this->Asset->Location->find('list');
        $this->set('locations', $locations);
        
        // unit codes
        $unit_codes = $this->Asset->UnitCode->find('list');
        $this->set('unit_codes', $unit_codes);
    }

/**
 * Buku Induk
 */
    function lists() {
        $this->layout = 'printhtml';
        Configure::write('debug', 2);
        
        $this->Asset->Behaviors->attach('Containable');
        $assets = $this->Asset->find('all', array(
            'contain' => array(
                'Item' => array(
                    'fields' => array(
                        'name', 'type', 'production_year', 'machine_number', 'morning_power_usage', 'night_power_usage',
                        'unit_id', 'price_per_unit', 'width', 'height', 'length', 'provider_id'
                    ),
                    'Unit' => array( 'fields' => array('name') ),
                    'Provider' => array( 'fields' => array('name') ),
                    'ServicePlan' => array( 'fields' => array('name') )
                ),
                'Location' => array('name'),
                'UnitCode' => array('name')
            )
        ));
        
        $records = array();
        $counter = 0;
        foreach ($assets as $key => $asset) {
            $records[$counter]['receipt_no'] = $asset['Asset']['receipt_no'];
            $records[$counter]['receipt_date'] = $asset['Asset']['receipt_date'];
            $records[$counter]['receipt_no_spk'] = $asset['Asset']['receipt_no_spk'];
            $records[$counter]['sp2d_no'] = $asset['Asset']['sp2d_no'];
            $records[$counter]['sp2d_date'] = $asset['Asset']['sp2d_date'];
            $records[$counter]['sp2d_klb'] = $asset['Asset']['sp2d_klb'];
            $records[$counter]['spm'] = $asset['Asset']['spm'];
            
            // init val for current record
            $records[$counter]['inventory_no'] = $asset['Asset']['inventory_no'];
            $records[$counter]['total'] = $asset['Asset']['total'];
            $records[$counter]['provider'] = $asset['Item']['Provider']['name'];
            $records[$counter]['service_plan'] = $asset['Item']['ServicePlan']['name'];
            $records[$counter]['item_name'] = $asset['Item']['name'];
            $records[$counter]['item_type'] = $asset['Item']['type'];
            $records[$counter]['item_year'] = $asset['Item']['production_year'];
            $records[$counter]['item_machine'] = $asset['Item']['machine_number'];
            $records[$counter]['item_morning_power'] = $asset['Item']['morning_power_usage'];
            $records[$counter]['item_night_power'] = $asset['Item']['night_power_usage'];
            $records[$counter]['item_unit'] = $asset['Item']['Unit']['name'];
            $records[$counter]['item_location'] = $asset['Location']['name'];
            $records[$counter]['item_unit_code'] = $asset['UnitCode']['name'];
            $records[$counter]['item_price'] = $asset['Item']['price_per_unit'];
            $records[$counter]['item_price_total'] = $asset['Item']['price_per_unit'] * $asset['Asset']['total'];
            $counter++;
        }
        $this->set('records', $records);
    }
    
    function lists_per_location() {
        if ( !isset($this->data['Asset']['date']) && !isset($this->data['location']) ) {
            $this->set('show_form', true);
            $locations = $this->Asset->Location->find('list');
            $this->set('locations', $locations);
        } else {
            $date = $this->data['Asset']['date']['year'] . '-' . $this->data['Asset']['date']['month'] . '-' .
                    $this->data['Asset']['date']['day'];
            $this->set('date', $date);
            
            $location = $this->data['location'];
            $location_name = $this->Asset->Location->find('first', array(
                'conditions' => array(
                    'id' => $location
                ), 'recursive' => -1, 'fields' => array(
                    'name', 'width', 'height', 'length'
                )
            ));
            $this->set('location_name', $location_name['Location']['name']);
            if ( $location_name['Location']['width'] && $location_name['Location']['width'] ) {
                $this->set('wide', $location_name['Location']['width']*$location_name['Location']['length']);
            }
            
            $this->set('show_form', false);
            //Configure::write('debug', 0);
            $this->layout = 'printhtml';
            
            $this->Asset->bindModel(array(
                'hasMany' => array(
                    'ItemService' => array(
                        'conditions' => array(
                            'ItemService.service_date <=' => $date
                        )
                    )
                )
            ));
            $this->Asset->Behaviors->attach('Containable');
            $assets = $this->Asset->find('all', array(
                'contain' => array(
                    'conditions' => array(
                        'Asset.location_id' => $location
                    ),
                    'Item' => array(
                        'fields' => array(
                            'name', 'type', 'production_year', 'machine_number',
                            'unit_id', 'price_per_unit', 'width', 'height', 'length', 'provider_id'
                        ),
                        'Unit' => array( 'fields' => array('name') )
                    ),
                    'ItemService' => array('service_for', 'service_for'),
                    'Location' => array('name'),
                    'UnitCode' => array('name')
                )
            ));
            
            pr($assets);
            die;
        }
    }
}
?>
