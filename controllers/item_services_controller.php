<?php
class ItemServicesController extends AppController {
    var $pageTitle    = 'Asset yang diservice';
    
    function add() {
        $this->__setAdditionals();
        parent::add();
    }
    
    function edit($id) {
        $this->__setAdditionals();
        parent::edit($id);
    }
    
    function __setAdditionals() {
        $this->ItemService->Asset->Behaviors->attach('Containable');
        $asset_items = $this->ItemService->Asset->find('all', array(
            'fields' => array('id', 'inventory_no', 'total'),
            'contain' => array(
                'Item' => array(
                    'fields' => array('name', 'type', 'production_year', 'machine_number')
                ),
                'Location' => array(
                    'fields' => array('name')
                )
            )
        ));
        
        // for select options
        $items = array(); 
        
        // for json
        $items_js = array();
        
        foreach ( $asset_items as $key => $item ) {
            $item_desc = '';
            if ( !empty($item['Item']['type']) ) {
                $item_desc .=  '(' . $item['Item']['type'];
            }
            if ( !empty($item['Item']['machine_number']) ) {
                if ( empty($item_desc) ) {
                    $item_desc .=  '(';
                } else {
                    $item_desc .=  ' - ';
                }
                $item_desc .=  $item['Item']['machine_number'];
            }
            if ( !empty($item_desc) ) {
                $item_desc = ' ' . $item_desc . ')';
            }
            $items[$item['Asset']['id']] = $item['Item']['name'] . $item_desc;
            $items_js[$item['Asset']['id']] = array(
                'location' => $item['Location']['name'],
                'inventory_no' => $item['Asset']['inventory_no'],
                'total' => $item['Asset']['total']
            );
        }
        $this->set('items', $items);
        $this->set('items_js', 'var item_details = ' . json_encode($items_js) . ';');
        $this->set('service_for', array(
            1 => 'Rusak berat', 2 => 'Perbaikan', 3 => 'Pemeliharaan rutin'
        ));
    }
}
?>
