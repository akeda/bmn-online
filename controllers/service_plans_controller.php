<?php
class ServicePlansController extends AppController {
    var $pageTitle = 'Service Plans';
    
    function add() {
        $this->__setAdditionals();
        parent::add();
    }
    
    function edit($id) {
        $this->__setAdditionals();
        parent::edit($id);
    }
    
    function __setAdditionals() {
        $this->set('periods', array(
            'month' => 'Month',
            'day' => 'Day',
        ));
    }
}
?>
